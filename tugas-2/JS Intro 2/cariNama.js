function searchName(callback, arr, pattern, maxOutput) {
    //filter tipe data string
    arr = arr.filter((value) => typeof value === "string" && value);
    
    //callback function searchLoop
    callback(arr, pattern, maxOutput);
}

function searchLoop(oldArr, pattern, maxOutput) {
    const newArr = oldArr.reduce((acc, value) => { 
        if (acc.found < maxOutput) {
            if (value.toLowerCase().includes(pattern.toLowerCase())) {
                acc.result.push(value);
                acc.found++;
                return acc;
            }
        }
        //mengembalikkan nilai dari parameter
        return acc;
    },{ result: [], found: 0 });

  console.info(newArr.result);
}
const person = [
    "Abigail", "", "Alexandra", "Alison",
    "Amanda", "Angela", "Bella",
    "Carol", "Caroline", "Carolyn",
    "Deirdre", "Diana", "Elizabeth",
    "Ella", "Faith", "Olivia", "Penelope",10,0,true,false,null,undefined];   

searchName(searchLoop,person," ",10);
