// {
//     //Array.filter
//     //Melakukan filter data dengan value > 5
//     const arrayFilter = [1,2,3,4,5,6,7,8,9,10];
//     const result = arrayFilter.filter( value => value >5 );
//     console.info(`Array filter result: ${result}`); //Array filter result: 6,7,8,9,10
// }

// {
//     //Array.reduce
//     //Melakukan penjumlahan pada seluruh data array
//     const arrayReduce = [1,2,3,4,5];
//     const result = arrayReduce.reduce((accumulator, value) => accumulator += value ,0)
//     console.info(`Array reduce result: ${result}`); //Array reduce result: 15
// }

// {
//     const arr = [1,1,1,1,1];
//     const result = arr.map(value => value + 1)
//     console.info(`Array map result: ${result}`);
// }

{
    //Array.splice
    //Memotong array dengan index dan jumlah data tertentu
    const array = ["Mikhael","Kevin"];
    array.splice(1,1) ;
    // console.info(`Array splice result: ${array}`); //Array splice result: Mikhael
}

{
    //String.slice
    //Memotong karakter tertentu sesuai index karakter pada string
    const kata = "Javascript";
    // console.info(`String slice result: ${kata.slice(0,4)}`); //String slice result: Java
}

{
    //Array.find
    //Hampir sama seperti filter, hanya saja yang diambil hanya nilai pertama yang memenuhi kondisi
    const arrFind = [10,11,13,14,16,17,20];
    const result = arrFind.find(value => value % 2 === 0);
    // console.info(`Array find result: ${result}`); //Array find result: 10
}

{
    // Array.includes
    //Mencari sebuah nilai dengan suatu kondisi yang menghasilkan return sebuah boolean
    const arrayIncludes = [1,5,7,9,10];
    const result = arrayIncludes.includes(7);
    // console.info(`Array includes result: ${result}`); //Array includes result: true
}

{
    //Array.findIndex
    //Sama seperti find namun saja nilai kembalinya merupakan sebuah index
    const arrIndex = [1,2,3,9,100];
    const result = arrIndex.findIndex(value => value === 3);
    // console.info(`Array find index result: ${result}`); //Array find index result: 2
}

{
    //String.repeat
    //Melakukan pengulangan terhadap sebuah instance objek sebanyak n
    const kata = "Kevin ";
    const result = kata.repeat(2);
    // console.info(`String repeat result: ${result}`); //String repeat result: Kevin Kevin
}

{
    //String.replace
    //Melakukan perubahan terhadap suatu string
    const kata = "Saya makan ayam hari ini";
    const result = kata.replace("ayam","sapi");
    // console.info(`String replace result: ${result}`);
}

{
    //value isNaN
    //Melakukan pengecekan terhadap suatu variable apakah variable tersebut adalah NaN (Not a number)
    const angka = Number("2");
    const kata = Number("Buah buahan");
//     console.info(`Angka adalah NaN? ${isNaN(angka)} 
// Kata adalah NaN? ${isNaN(kata)}`);// Kata adalah NaN? true && //Angka adalah NaN? false
}