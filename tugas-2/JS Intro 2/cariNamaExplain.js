function searchName(callback,arr, pattern, maxOutput){
    //deklarasi i untuk iterasi
    let i = 0;
    //melakukan deklarasi ulang parameter arr dengan value berupa hasil dari instance method filter
    arr = arr.filter((value) => {
        //flow ketika i kurang dari maxoutput
        if(i<maxOutput){
            //flow untuk menyaring isi dari var arr sebelumnya
            //flow ini menyaring data berupa string dan valuenya yang mengandung pattern
            if(typeof value === "string" && value.toLowerCase().includes(pattern.toLowerCase())){
                i++;
                return value;
            }
        }
    });
    
    //melakukan output dengan memanggil function display
    callback(arr);
}

function displayResult(array){
    console.info(array);
}

const person = [
    "Abigail", "Alexandra", "Alison",
    "Amanda", "Angela", "Bella",
    "Carol", "Caroline", "Carolyn",
    "Deirdre", "Diana", "Elizabeth",
    "Ella", "Faith", "Olivia", "Penelope","El Pablo",10,0,true,false,null,undefined
];  

searchName(displayResult,person, "p", 3);
