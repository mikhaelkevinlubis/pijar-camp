function seleksiNilai(nilaiAwal,nilaiAkhir,arr){
    if(typeof nilaiAwal === "number" && typeof nilaiAkhir === "number"){
        const newArr = arr.filter(value => typeof value === "number" && value > nilaiAwal && value < nilaiAkhir);

        if(nilaiAwal>nilaiAkhir){
            console.info("Nilai akhir harus lebih besar dari nilai awal");
        }else if(arr.length < 5){
            console.info("Jumlah angka dalam dataArray harus lebih dari 5");
        }else if(newArr.length === 0){
            console.info("Nilai tidak ditemukan");
        }else{
            console.info(newArr.sort((a,b)=> a-b));
        /* 
            [14,17,8]
            14>17 = stay
            17>8 = swap => [14,8,17]
            14>8 = swap => [8,14,17]
            14>17 = stay 
            Done
        */
        }
    }else{
        console.info("Nilai awal dan nilai akhir harus berupa number!");
    }
    
}

seleksiNilai(5, 20 , [2, 25, 4, 14, 17, 30, 8, "7",false,{},"kata"]);
// seleksiNilai(15, 3 , [2, 25, 4, 14, 17, 30, 8])
// seleksiNilai(5, 17 , [2, 25, 4])
// seleksiNilai(5, 17 , [2, 25, 4, 1, 30, 18])
// seleksiNilai(5, false , [2, 25, 4, 1, 30, 18])

