const cekHariKerja = day => {
    return new Promise( (resolve, reject) => {
        setTimeout(() => {
            try{
                const dataDay = ['senin','selasa','rabu','kamis','jumat'];
                let cek = dataDay.find(item => item === day.toLowerCase());
                if(cek){
                    resolve(`Hari ${day} adalah hari kerja`);
                }else{
                    reject(new Error(`Hari ${day} bukanlah hari kerja`));
                }
            } catch(error){
                reject(new Error(`Terdapat kesalahan pada parameter`));
            }
        },3000);
    });
}
        

cekHariKerja("minggu")
.then(res => console.info(res))
.catch(error => console.error(error.message));

/*
    try & catch -> synchronous
                -> try digunakan sebagai eksekusi pertama saat function dijalankan untuk mencoba proses didalamnya
                -> catch digunakan sebagai eksekusi kedua yang berperan mengambil error saat try tidak dapat dilakukan
    
    then & catch -> asynchronous
                ->  then digunakan untuk mengambil data yang diproses pada function (dilempar oleh resolve)
                ->catch hampir sama seperti catch pada try&catch namun untuk catch pada promise(yang dilempar oleh reject)
*/
