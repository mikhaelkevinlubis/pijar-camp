//tugas no 4.
import fetch from "node-fetch";

const tryFetch = async (params = "") => {
  try{
    const url = `https://jsonplaceholder.typicode.com/users/${params}`;
    const response = await fetch(url);
    if (response.status !== 200) {
      return new Error(response.status);
    }

    const getData = await response.json();

    if (typeof params === "number") {
      return getData.name;
    }
    return getData;
  } catch (error){
    return error;
  }
};

tryFetch()
  .then((res) => {
    if(Array.isArray(res) && res.length > 0){
      res.map(value => console.info(value.name))
    }else{
      console.info(res);
    }
  })
  .catch((error) => {
    console.info(error);
    if(error === "404") {
      console.info("Data tidak ditemukan...")
    }else{
      console.info(error.message);
    };
  });