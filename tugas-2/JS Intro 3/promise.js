const promiseOne = (...data) => {
    return new Promise ((resolve, reject) => {
        setTimeout(()=> {
                const checker = data.map(value => typeof value === "number").reduce((result,value) => result+=value);
                if(checker === data.length){
                    const ganjilGenap = data.reduce((result,value) => {
                    if (value % 2 === 0) {
                        result.genap.push(value);
                        return result;
                    } else {
                        result.ganjil.push(value);
                        return result;
                    }
                    },{ganjil: [], genap: []});
                    ganjilGenap ? resolve(ganjilGenap) : reject(new Error("Terjadi kesalahan pada proses"));
                }else{
                    reject(new Error("Terdapat kesalahan tipe data pada parameter inputan"));
                }
        },3000);

    });
}

// promiseOne(1,2,3,4,5,"6",7,8,9,10).then(res => console.info(res)).catch(error => console.info(error.message));

const promiseTwo = (keyword) => {
    return new Promise ((resolve,reject) => {
        setTimeout(() => {
            const items = [{
                namaBarang: "Sabun",
                hargaBarang: 5000
            },
            {
                namaBarang: "Sikat Gigi",
                hargaBarang: 6000
            },
            {
                namaBarang: "Shampoo Sachet",
                hargaBarang: 1000
            },
            {
                namaBarang: "Shampoo Botol",
                hargaBarang: 12000
            }];

                if(keyword && typeof keyword === "string"){
                    const cariBarang = items.filter(value => (value.namaBarang.toLowerCase()).includes(keyword.toLowerCase()));
                    cariBarang.length ? resolve(cariBarang) : reject(new Error("Opps, terjadi kesalahan pada proses pencarian..."));
                }else{
                    reject(new Error("Opps, terjadi kesalah. Silahkan cek parameter inputan anda..."));
                }
        },3000);
    });
}

promiseTwo(false).then(res => console.info(res)).catch(error => console.info(error.message));