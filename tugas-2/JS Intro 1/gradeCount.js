const isNewRule = (nilai) => {
    const resultRule = [];
    resultRule.push(nilai !== "" ? true : false);
    resultRule.push(nilai >= 0 ? true : false);
    resultRule.push(typeof nilai === "number" ? true : false); 
    resultRule.push(nilai !== false ? true : false);
    resultRule.push(nilai !== null ? true : false);
    resultRule.push(nilai !== undefined ? true : false);
    resultRule.push(nilai !== NaN ? true : false);
    
    const ruleChecker = resultRule.reduce((result, value) => result += value);
    return ruleChecker === resultRule.length ? true : false
}

const nilaiIndonesia = 80;
const nilaiInggris = 90;
const nilaiMTK = 89;
const nilaiIPA = 69;

if(isNewRule(nilaiIndonesia) && isNewRule(nilaiInggris) && isNewRule(nilaiMTK) && isNewRule(nilaiIPA)){
    const average = (nilaiIndonesia + nilaiInggris + nilaiMTK +nilaiIPA) / 4;
    let grade;
    average >= 90 ? grade = "A"
    : average >= 80 ? grade = "B"
    : average >= 70 ? grade = "C"
    : average >= 60 ? grade = "D" 
    : grade = "E";

    console.info(`Rata-rata: ${average} Grade: ${grade}`);
}else{
        console.info("Data harus terisi semua dan merupakan angka yang lebih besar dari 0");
}


