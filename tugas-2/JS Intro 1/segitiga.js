const printSegitiga = 5; 

if(typeof printSegitiga === "number"){
    for(let row = printSegitiga; row>0; row--){
        let temp = "";
        for(let col = 1; col <= row; col++){
            temp+=col + " ";
        }
        console.info(temp);
    }
}else{
    console.info("Data harus berupa tipe data number");
}

