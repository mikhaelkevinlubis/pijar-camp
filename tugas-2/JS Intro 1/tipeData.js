const biodata = {
    name: "Mikhael Kevin",
    age: 24,
    hobbies: ["Writing, Gaming, Watching movie"],
    isMaried: false,
    schoolList: [{
        name: "SMKN 11 Bandung",
        yearIn: 2012,
        yearOut: 2015,
        major: "RPL"
    },
    {
        name: "UNJANI",
        yearIn: 2015,
        yearOut: 2021,
        major: "Informatika"
    }],
    skills: [{
        skillName: "Laravel",
        level: "Beginner"
    },
    {
        skillName: "JavaScript",
        level: "Beginner"
    }],
    interestInCoding: true
}

console.info(biodata);